from app.models import db, Player, Team, Game
from app.tests import result, ModelTests
import unittest
import json

PAGE_SIZE = 25

def json_format(obj):
    return json.dumps(obj, indent=4)

def players():
    """Returns a list of ids for players in the NBA"""
    return json_format([id[0] for id in db.session.query(Player.player_id)])

def players_by_page(page_num):
    """Returns a page of player stats"""
    offset = (page_num - 1) * PAGE_SIZE
    results = db.session.query(Player).offset(offset).limit(PAGE_SIZE)
    return json_format([player.values() for player in results])

def player_by_id(player_id):
    """Returns stats for the player with the given id"""
    return json_format(db.session.query(Player).filter(Player.player_id == player_id).first().values())

def teams():
    """Returns stats for all the teams in the NBA"""
    results = db.session.query(Team)
    return json_format([team.values() for team in results])

def team_by_id(teams_id):
    """Returns stats for the team with the given id"""
    return json_format(db.session.query(Team).filter(Team.team_id == teams_id).first().values())

def team_logo(team_id):
    """Returns the logo of the team with a given id"""
    pass

def games():
    """Returns a list of ids for games in the NBA"""
    return json_format([id[0] for id in db.session.query(Game.game_id)])

def games_by_page(page_num):
    """Returns a page of game stats"""
    offset = (page_num - 1) * PAGE_SIZE
    results = db.session.query(Game).offset(offset).limit(PAGE_SIZE)
    return json_format([game.values() for game in results])

def game_by_id(game_id):
    """Returns stats for the game with the given id"""
    return json_format(db.session.query(Game).filter(Game.game_id == game_id).first().values())

def all_players():
    """Returns stats for all players"""
    return json_format([player.values() for player in db.session.query(Player).all()])

def all_teams():
    """Returns stats for all teams"""
    return json_format([team.values() for team in db.session.query(Team).all()])

def all_games():
    """Returns stats for all games"""
    return json_format([game.values() for game in db.session.query(Game).all()])

def tests():
    """Returns test results"""
    suite = unittest.TestLoader().loadTestsFromTestCase(ModelTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
    return json_format(result)