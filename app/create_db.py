#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/create_db.py
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import json
from models import app, db, Player, Team, Game
import requests
import time


# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create players
# ------------
def create_players(page):
    """
    populate player table
    """

    get = "https://www.balldontlie.io/api/v1/players?page="
    response = requests.get(get + str(page))
    player = response.json()
    #print(data)
    #player = load_json(response)

    for onePlayer in player['data']:
        player_id = onePlayer['id']
        stat_get = "https://www.balldontlie.io/api/v1/season_averages?season=2019&player_ids[]="
        stat_response = requests.get(stat_get + str(onePlayer['id']))
        stats = stat_response.json()
        if 'data' in stats and len(stats['data']) < 1:
            space = ""
        else:
            space = " "
            name = onePlayer['first_name'] + space + onePlayer['last_name']
            team = onePlayer['team']
            teamID = team['id']
            position = onePlayer['position']
            height_feet = onePlayer['height_feet']
            height_inches = onePlayer['height_inches']
            weight = onePlayer['weight_pounds']
            newTeam = db.session.query(Team).get(teamID)
            for oneStats in stats['data']:
                games = oneStats['games_played']
                points = oneStats['pts']
                assists = oneStats['ast']
                rebounds = oneStats['reb']
                steals = oneStats['stl']
                blocks = oneStats['blk']


                

            newPlayer = Player( player_id = player_id, name = name, team = newTeam, position = position, height_feet = height_feet, height_inches = height_inches, weight = weight, games = games, points = points, assists = assists, rebounds = rebounds, steals = steals, blocks = blocks)
            # After I create the player, I can then add it to my session. 
            db.session.add(newPlayer)
            # commit the session to my DB.
            db.session.commit()

# ------------
# create teams
# ------------

def create_teams():
    """
    populate teams table
    """

    response = requests.get("https://www.balldontlie.io/api/v1/teams")
    team = response.json()
    #print(data)
    #player = load_json(response)

    for oneTeam in team['data']:
        team_id = oneTeam['id']
        #visitor_id = oneTeam['id']
        name = oneTeam['full_name']
        city = oneTeam['city']
        abbr = oneTeam['abbreviation']
        division = oneTeam['division']
        conference = oneTeam['conference']
        
		
        newTeam = Team( team_id = team_id, name = name, city = city, abbr = abbr, division = division, conference = conference)
        
        # After I create the team, I can then add it to my session. 
        db.session.add(newTeam)
        # commit the session to my DB.
        db.session.commit()

# ------------
# create games
# ------------
def create_games(page):
    """
    populate games table
    """

    get = "https://www.balldontlie.io/api/v1/games?seasons[]=2019&page="
    response = requests.get(get + str(page))
    game = response.json()
    #print(data)
    #player = load_json(response)

    for oneGame in game['data']:
        game_id = oneGame['id']
        date = oneGame['date']
        home = oneGame['home_team']
        homeID = home['id']
        visitor = oneGame['visitor_team']
        visitorID = visitor['id']
        home_score = oneGame['home_team_score']
        visitor_score = oneGame['visitor_team_score']
        newHome = db.session.query(Team).get(homeID)
        newVisitor = db.session.query(Team).get(visitorID)
        
		
        newGame = Game(game_id = game_id, date = date, homeID = homeID, visitorID = visitorID, home_score = home_score, visitor_score = visitor_score)
        #newGame = Game(game_id = game_id, date = date, homeID = newHome, home_score = home_score, visitor_score = visitor_score)
        
        # After I create the game, I can then add it to my session. 
        db.session.add(newGame)
        # commit the session to my DB.
        db.session.commit()
        newGame.GameTeam.append(newHome)
        db.session.commit()
        newGame.GameTeam.append(newVisitor)
        db.session.commit()
        #for u in newHome.get(players).all():
        #    newGame.GamePlayer.append(u)


def player_game_relation():
    query = db.session.query(Player)
    for onePlayer in query:
        teamID = onePlayer.teamID
        for u in db.session.query(Game).filter(Game.homeID == teamID).all():
            u.GamePlayer.append(onePlayer)
            db.session.commit()
        for u in db.session.query(Game).filter(Game.visitorID == teamID).all():
            u.GamePlayer.append(onePlayer)
            db.session.commit()








#create_teams()

#create_games(1)

#for x in range(1, 50):
#    create_games(x)

for x in range(120, 103, -1):
    print(x)
    create_players(x)
    time.sleep(60)

#create_players(131)
#player_game_relation()



#    time.sleep(20)
