#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os


app = Flask(__name__, static_folder='../react-app/build', static_url_path='/')
#app = Flask(__name__)
#static_folder='../react-app/build'


#app = Flask(__name__, static_folder="../react-app/build", template_folder="../react-app/build")




#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:smshah@localhost:5432/bookdb')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:smshah@104.198.145.33/postgres')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)




link = db.Table('link',
   db.Column('team_id', db.Integer, db.ForeignKey('team.team_id')), 
   db.Column('game_id', db.Integer, db.ForeignKey('game.game_id'))
   ) 

   
association_table = db.Table('association',
    db.Column('player_id', db.Integer, db.ForeignKey('player.player_id')),
    db.Column('game_id', db.Integer, db.ForeignKey('game.game_id'))
) 

# ------------
# Player
# ------------
class Player(db.Model):
    """
    Player class has seven attrbiutes 
    id
    name
    teamID
    position
    height_feet
    height_inches
    weight
    """
    __tablename__ = 'player'
	
    player_id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    #teamID = db.Column(db.Integer)
    # This property refer to the "id" property in the "team" table
    teamID = db.Column(db.Integer, db.ForeignKey('team.team_id'))
    position = db.Column(db.String(80))
    height_feet = db.Column(db.Integer)
    height_inches = db.Column(db.Integer)
    weight = db.Column(db.Integer)
    games = db.Column(db.Integer) 
    points = db.Column(db.Float) 
    assists = db.Column(db.Float) 
    rebounds = db.Column(db.Float) 
    steals = db.Column(db.Float) 
    blocks = db.Column(db.Float) 
    game_list = db.relationship("Game",
                    secondary=association_table,
                    backref="GamePlayer")

    def values(self):
        height = (str(self.height_feet) + ' - ' + str(self.height_inches))
        if self.height_feet is not None and self.height_inches is not None:
            height_inches = (12 * self.height_feet + self.height_inches)
        else:
            height_inches = ''

        return dict(
                id = self.player_id,
                name = self.name,
                position = self.position,
                height = height,
                height_inches = height_inches,
                weight = self.weight,
                gamesplayed = self.games,
                pointspg = self.points,
                reboundspg = self.rebounds,
                assistspg = self.assists,
                stealspg = self.steals,
                blockspg = self.blocks,
                teamId = self.teamID)
    
    


# ------------
# Team
# ------------

class Team(db.Model):
    """
    Team class has six attrbiutes 
    id
    name
    city
    abbreviation
    division
    conferencex
    """
    __tablename__ = 'team'
	
    team_id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    city = db.Column(db.String(80))
    abbr = db.Column(db.String(80))
    division = db.Column(db.String(80))
    conference = db.Column(db.String(80))
    players = db.relationship('Player', backref = 'team')
    games = db.relationship('Game', secondary = 'link', backref='GameTeam')
    #home_games = db.relationship('Game', backref = 'team')
    #visitor_games = db.relationship('Game', backref = 'team')

    def values(self):
        return dict(
                id = self.team_id,
                name = self.name,
                abbr = self.abbr,
                city = self.city,
                conference = self.conference,
                division = self.division,
                playerIds = [player.player_id for player in \
                        db.session.query(Player).filter(Player.teamID == self.team_id)])


# ------------
# Games
# ------------
class Game(db.Model):
    """
    Game class has six attrbiutes 
    id
    date
    homeID
    visitorID
    home_score
    visitor_score
    
    """
    __tablename__ = 'game'
	
    game_id = db.Column(db.Integer, primary_key = True)
    date = db.Column(db.String(80), nullable = False)
    #homeID = db.Column(db.Integer, db.ForeignKey('team.team_id'))
    #visitorID = db.Column(db.Integer, db.ForeignKey('team.team_id'))
    home_score = db.Column(db.Integer)
    visitor_score = db.Column(db.Integer)
    homeID = db.Column(db.Integer)
    visitorID = db.Column(db.Integer)
    
    #homeID = db.Column(db.Integer, db.ForeignKey('team.team_id'), nullable=False)
    #visitorID = db.Column(db.Integer, db.ForeignKey('team.team_id'), nullable=False)
    #home_game = db.relationship("Team", foreign_keys=[homeID], backref = 'team')
    #visitor_game = db.relationship("Team", foreign_keys=[visitorID])
    
    def values(self):
        return dict(
                id = self.game_id,
                date = self.date,
                homeTeamId = self.homeID,
                homeTeamScore = self.home_score,
                awayTeamId = self.visitorID,
                awayTeamScore = self.visitor_score)

