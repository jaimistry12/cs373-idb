openapi: 3.0.0
info:
  version: 1.5.0
  title: stats
  description: 'A RESTful API for NBA stats'
servers:
  - url: 'https://hoopscoop.me/api'
paths:
  /players:
    get:
      summary: indices of player ids
      description: Returns a list of ids for players in the NBA
      responses:
        '200':
          description: An array of player ids
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdsList'
  /players/page/{number}:
    parameters:
      - in: path
        name: number
        schema:
          type: integer
          minimum: 1
          maximum: 131
          default: 1
        description: the page number
    get:
      summary: pages of player stats
      description: Returns a page of player stats
      responses:
        200:
          description: a page of player stats
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PlayersList'
  /players/id/{id}:
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: integer
          minitems: 1
          maxitems: 3268
          default: 1
        description: a player's id
    get:
      summary: player stats by id
      description: Returns stats for the player with the given id
      responses:
        200:
          description: stats for the player with the given id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Player'
  /teams:
    get:
      summary: all team stats
      description: Returns stats for all the teams in the NBA
      responses:
        200:
          description: stats for all the teams
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TeamsList'
  /teams/id/{id}:
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: integer
          minitems: 1
          maxitems: 30
          default: 1
        description: a team's id
    get:
      summary: team stats by id
      description: Returns stats for the team with the given id
      responses:
        200:
          description: stats for the team with the given id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Team'
  /games:
    get:
      summary: indices of game ids
      description: Returns a list of ids for games in the NBA
      responses:
        '200':
          description: An array of game ids
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdsList'
  /games/page/{number}:
    parameters:
      - in: path
        name: number
        schema:
          type: integer
          minimum: 1
          maximum: 1951
          default: 1
        description: the page number
    get:
      summary: pages of game stats
      description: Returns a page of game stats
      responses:
        200:
          description: a page of game stats
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GamesList'
  /games/id/{id}:
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: integer
          minitems: 1
          maxitems: 48754
          default: 1
        description: a game's id
    get:
      summary: game stats by id
      description: Returns stats for the game with the given id
      responses:
        200:
          description: stats for the game with the given id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Game'
components:
  schemas:
    IdsList:
      type: array
      items:
        type: integer
      uniqueItems: true
    Player:
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        position:
          type: string
        height:
          type: string
        weight:
          type: integer
        gamesplayed:
          type: integer
        pointspg:
          type: number
        reboundspg:
          type: number
        assistspg:
          type: number
        stealspg:
          type: number
        blockspg:
          type: number
        teamId:
          type: integer
      required:
        - id
        - name
        - position
        - height
        - weight
        - gamesplayed
        - pointspg
        - reboundspg
        - assistspg
        - stealspg
        - blockspg
        - teamId
    PlayersList:
      type: array
      items:
        $ref: '#/components/schemas/Player'
    Team:
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        abbreviation:
          type: string
        city:
          type: string
        conference:
          type: string
        division:
          type: string
        playerIds:
          type: array
          items:
            type: integer
      required:
        - id
        - name
        - abbreviation
        - city
        - conference
        - division
        - playerIds
    TeamsList:
      type: array
      items:
        $ref: '#/components/schemas/Team'
    Game:
      type: object
      properties:
        id:
          type: integer
        date:
          type: string
          format: date
        homeTeamId:
          type: integer
        homeTeamScore:
          type: integer
        awayTeamId:
          type: integer
        awayTeamScore:
          type: integer
    GamesList:
      type: array
      items:
        $ref: '#/components/schemas/Game'

