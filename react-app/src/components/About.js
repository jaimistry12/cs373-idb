import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import url from '../url.js';
import '../css/About.css';
import ayan from '../images/picture-of-ayan.jpeg';
import lina from '../images/picture-of-lina.jpg';
import jai from '../images/picture-of-jai.jpeg';
import shrey from '../images/picture-of-shrey.png';
import kevin from '../images/picture-of-kevin.jpg';
import axios from 'axios';

class About extends React.Component {
  state = { testsRan: false, results: {} };

  async runTests() {
    let res = await axios.get(url + 'api/tests');
    this.setState({ results: res.data.slice(0, 10), testsRan: true });
  }

  getTests = () => {
    if (!this.state.testsRan)
      return (
        <button
          className='btn btn-dark btn-lg tests-button-custom'
          onClick={() => this.runTests()}
        >
          Run Tests
        </button>
      );
    return (
      <div>
        <ol>
          {this.state.results.map((result) => {
            let test = result.substring(0, result.length - 4);
            let output = result.substring(result.length - 4);
            return (
              <li>
                {test}
                <span className='test-output'>{output}</span>
              </li>
            );
          })}
        </ol>
        <p className='test-note'>
          *Only the first 10 test results are displayed*
        </p>
      </div>
    );
  };

  render() {
    return (
      <div className='bg-dark' style={{ color: 'white' }}>
        {/******************** JUMBOTRON ********************/}
        <div className='jumbotron jumbotron-fluid text-center bg-secondary jumbo-custom'>
          <div className='container'>
            <h1 className='display-3 jumbo-title-custom'>
              <b>The Hooper Scoopers</b>
            </h1>
            <ul className='member-names'>
              <li>
                <a href='#ayan'>Ayan Karmakar</a>
              </li>
              <li>
                <a href='#lina'>Lina Ly</a>
              </li>
              <li>
                <a href='#jai'>Jai Mistry</a>
              </li>
              <li>
                <a href='#shrey'>Shrey Shah</a>
              </li>
              <li>
                <a href='#kevin'>Kevin Shin</a>
              </li>
            </ul>
          </div>
        </div>
        <div className='container'>
          {/******************** TEAM STATISTICS ********************/}
          <div className='position-relative team-stats-custom'>
            <div className='team-statistics'>
              <h2>Team Statistics</h2>
              <ul className='team-stats'>
                <li>Total Issues: 80</li>
                <li>Total Commits: 188</li>
                <li>Total Unit Tests: 10</li>
              </ul>
            </div>
            <div className='team-links'>
              <h2>Relevant Links</h2>
              <ul className='relevant-links'>
                <li>
                  <a
                    href='https://gitlab.com/ayan_karmakar/cs373-idb/-/wikis/home'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    GitLab Wiki
                  </a>
                </li>
                <li>
                  <a
                    href='https://gitlab.com/ayan_karmakar/cs373-idb'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    GitLab Repo
                  </a>
                </li>
                <li>
                  <a
                    href='https://documenter.getpostman.com/view/11805826/T1DiEfDG'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    Postman API
                  </a>
                </li>
                <li>
                  <a
                    href='https://gitlab.com/ayan_karmakar/cs373-idb/-/issues'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    GitLab Issue Tracker
                  </a>
                </li>
                <li>
                  <a
                    href='https://speakerdeck.com/smshah10/the-hoop-scoop'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    Speaker Deck Presentation
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/******************** DEVELOPMENT TOOLS ********************/}
          <div className='container position-relative development-tools-custom'>
            <h2>Development Tools</h2>
            <ul className='development-stats'>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://flask.palletsprojects.com/en/1.1.x/'
                >
                  Flask
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://cloud.google.com/gcp/?utm_source=google&utm_medium=cpc&utm_campaign=na-US-all-en-dr-bkws-all-all-trial-p-dr-1008076&utm_content=text-ad-lpsitelinkCCexp2-any-DEV_c-CRE_113120493247-ADGP_Hybrid+%7C+AW+SEM+%7C+BKWS+%7C+US+%7C+en+%7C+Multi+~+Cloud+Platform-KWID_43700011014879364-kwd-10876442192&utm_term=KW_cloud%20platform-ST_cloud+platform&gclid=CjwKCAjw88v3BRBFEiwApwLevWQ6DIGCmURFlTZhv-HhAYxTt55WT9npCkTsKPLl2LihhkkZWxbTChoC-gQQAvD_BwE'
                >
                  Google Cloud Platform
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://gitlab.com/'
                >
                  GitLab
                </a>
              </li>
            </ul>
            <ul className='development-stats'>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://getbootstrap.com/'
                >
                  Bootstrap
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://nc.me/'
                >
                  Namecheap
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://www.postman.com/'
                >
                  Postman
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://slack.com/'
                >
                  Slack
                </a>
              </li>
            </ul>
          </div>
          {/******************** DATA SOURCES ********************/}
          <div className='container position-relative data-sources-custom'>
            <h2>Data Sources</h2>
            <ul className='data-sources-list'>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://www.balldontlie.io/#players'
                >
                  Players
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://www.balldontlie.io/#teams'
                >
                  Teams
                </a>
              </li>
              <li>
                <a
                  target='_blank'
                  rel='noopener noreferrer'
                  href='https://www.balldontlie.io/#games'
                >
                  Games
                </a>
              </li>
            </ul>
            <p>
              *All our data was obtained from the players, teams, and games
              section of the balldontlie API. No API key is required.
            </p>
          </div>
          {/******************** Unit Tests  ********************/}
          <div className='container position-relative unit-tests-custom'>
            <h2>Unit Tests</h2>
            <div>{this.getTests()}</div>
          </div>
          {/******************** INDIVIDUAL PROFILES ********************/}
          {/****** AYAN ******/}
          <a name='ayan'></a>
          <div className='position-relative profile-custom'>
            <img className='profile-photo' src={ayan} alt='picture of Ayan' />
            <div className='profile-info'>
              <p>
                <b>Name:</b> Ayan Karmakar
              </p>
              <p>
                <b>Bio:</b> Hi! My name is Ayan. I am a third year CS major and
                I love playing chess.
              </p>
              <p>
                <b>Major Responsibilities:</b> Front End: GUI Design, React
                Migration and Search
              </p>
            </div>
            <ul className='profile-stats'>
              <li>Commits: 46</li>
              <li>Issues: 27</li>
              <li>Unit Tests: 1</li>
            </ul>
          </div>
          {/****** LINA ******/}
          <a name='lina'></a>
          <div className='position-relative overflow-hidden profile-custom'>
            <div className='profile-info'>
              <p>
                <b>Name:</b> Lina Ly
              </p>
              <p>
                <b>Bio:</b> Hi, my name is Lina and I'm a third year CS major. I
                am a huge fan of tetris.
              </p>
              <p>
                <b>Major Responsibilities: </b>GCP Deployment, API Changes
              </p>
            </div>
            <img className='profile-photo' src={lina} alt='picture of Lina' />
            <ul className='profile-stats'>
              <li>Commits: 24</li>
              <li>Issues: 14</li>
              <li>Unit Tests: 6</li>
            </ul>
          </div>
          {/****** JAI ******/}
          <a name='jai'></a>
          <div className='position-relative overflow-hidden profile-custom'>
            <img className='profile-photo' src={jai} alt='picture of Jai' />
            <div className='profile-info'>
              <p>
                <b>Name:</b> Jai Mistry
              </p>
              <p>
                <b>Bio:</b> Hello! My name is Jai Mistry. I am a third year CS
                major and I am a huge basketball fan.
              </p>
              <p>
                <b>Major Responsibilities: </b> Front End: Flask Integration,
                React Pagination and Search
              </p>
            </div>
            <ul className='profile-stats'>
              <li>Commits: 40</li>
              <li>Issues: 20</li>
              <li>Unit Tests: 1</li>
            </ul>
          </div>
          {/****** SHREY ******/}
          <a name='shrey'></a>
          <div className='position-relative overflow-hidden profile-custom'>
            <div className='profile-info'>
              <p>
                <b>Name:</b> Shrey Shah
              </p>
              <p>
                <b>Bio:</b> Hey my name is Shrey and I am a third year CS
                student. I want a cat.
              </p>
              <p>
                <b>Major Responsibilities: </b>Backend: Database
              </p>
            </div>
            <img className='profile-photo' src={shrey} alt='picture of Shrey' />
            <ul className='profile-stats'>
              <li>Commits: 48</li>
              <li>Issues: 9</li>
              <li>Unit Tests: 1</li>
            </ul>
          </div>
          {/****** KEVIN ******/}
          <a name='kevin'></a>
          <div className='position-relative overflow-hidden profile-custom'>
            <img className='profile-photo' src={kevin} alt='picture of Kevin' />
            <div className='profile-info'>
              <p>
                <b>Name:</b> Kevin Shin
              </p>
              <p>
                <b>Bio:</b> I'm a fourth-year CS/Math student, and I most enjoy
                card games.
              </p>
              <p>
                <b>Major Responsibilities: </b>API Design
              </p>
            </div>
            <ul className='profile-stats'>
              <li>Commits: 30</li>
              <li>Issues: 10</li>
              <li>Unit Tests: 1</li>
            </ul>
          </div>
        </div>
        <div className='end-of-page'></div>
      </div>
    );
  }
}

export default About;
