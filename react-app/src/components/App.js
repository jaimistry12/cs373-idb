import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from '../history';

import Navbar from './Navbar';
import Home from './Home';
import About from './About';
import Players from './Players';
import Teams from './Teams';
import Games from './Games';
import Player from './Player';
import Team from './Team';
import Game from './Game';
import Flights from './Flights';
import Search from './Search';

const App = () => {
  return (
    <Router history={history}>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/about' component={About} />
        <Route path='/search' component={Search} />
        <Route path='/players' component={Players} />
        <Route path='/teams' component={Teams} />
        <Route path='/games' component={Games} />
        <Route path='/player' component={Player} />
        <Route path='/team' component={Team} />
        <Route path='/game' component={Game} />
        <Route path='/flights' component={Flights} />
      </Switch>
    </Router>
  );
};

export default App;
