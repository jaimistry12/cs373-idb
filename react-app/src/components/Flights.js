import React from 'react';
import '../css/Models.css';
import teamsPic from '../images/flights-page-picture.png';
import axios from 'axios';

export default class Flights extends React.Component {
  state = {
    data: [],
    done: false,
  };

  componentDidMount() {
    var get_flights_to = function(airport_id, good_flights, callback) {
      // gets a list of flights to the given airport and appends them to the good_flights array
      // optionally calls the callback function when finished
      axios.get('http://aeroinfo.me/api/airports/' + airport_id)
        .then(airport_res => {  // get list of flights
          var remaining_flights = airport_res.data.flights.length;
          airport_res.data.flights.map(flight => {    // for each flight
            axios.get('http://aeroinfo.me/api/flights/' + flight.flight_id)
              .then(flight_res => { // get flight data
                if(flight_res.data.arrival_airport === airport_res.data.airport_name)    // if flight is in right direction
                  good_flights.push(flight_res.data);
                if((--remaining_flights === 0) && (typeof callback !== 'undefined'))
                  callback(good_flights);
              }).catch(console.log);
          });
        }).catch(console.log);
    };

    // create an empty array
    var good_flights = [];
    // populate it with flights to orlando international
    get_flights_to(3335, good_flights, good_flights_partial => {
      // and to orlando sanford international
      get_flights_to(4845, good_flights_partial, good_flights_final => {
        // then update state with the returned data
        this.setState({ data: good_flights_final});
        this.setState({ done: true });
      });
    });
  }

  render() {
    if (!this.state.done) {
      return <div></div>;
    }
    return (
      <div>
        {/********************** TABLE **********************/}
        <div className='container model-container-custom'>
          <h2>Looking to join us in Orlando?</h2>
          <div className='table-responsive text-center'>
            <table
              className='table table-dark table-striped table-sm'
              style={{ backgroundColor: '#111' }}
            >
              <thead>
                <tr>
                  <th>Flight Number</th>
                  <th>Date</th>
                  <th>Departure Airport</th>
                  <th>Arrival Airport</th>
                </tr>
              </thead>

              <tbody>
                {this.state.data.map((flight) => (
                  <tr>
                    <td>{flight.flight_iata}</td>
                    <td>{flight.flight_date}</td>
                    <td>{flight.departure_airport}</td>
                    <td>{flight.arrival_airport}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <img src={teamsPic} alt='airplane' />
        </div>
      </div>
    );
  }
}
