import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Game.css';
import axios from 'axios';
import url from '../url.js';

export default class Game extends React.Component {
  state = {
    data: {},
    playerData: [],
    home_team: [],
    away_team: [],
    p_done: false,
    done: false,
  };

  async componentDidMount() {
    let res = await axios.get(
      url + `api/games/id/${this.props.location.gameId}`
    );
    this.setState({ data: res.data });

    let homeTeam = await axios.get(
      url + `api/teams/id/${this.state.data.homeTeamId}`
    );
    let awayTeam = await axios.get(
      url + `api/teams/id/${this.state.data.awayTeamId}`
    );
    this.setState({ home_team: homeTeam.data, away_team: awayTeam.data });

    res = await axios.get(url + 'api/all/players');
    this.setState({ playerData: res.data, done: true });
  }

  render() {
    if (!this.state.done) return <div></div>;

    let homeTeamPlayerList = this.state.playerData.filter(
      (player) => player.teamId === this.state.data.homeTeamId
    );
    let visitorTeamPlayerList = this.state.playerData.filter(
      (player) => player.teamId === this.state.data.awayTeamId
    );

    return (
      <div>
        {/******************** GAME SUMMARY ********************/}
        <Link className='btn btn-dark back-button-custom' to='/games'>
          &laquo; Back To Games
        </Link>
        <div className='container'>
          <div className='card-deck text-center card-deck-custom'>
            <div className='container'>
              <div className='row'>
                <div className='col-sm-1'></div>
                <div className='card mb-5 shadow-sm bg-dark col-sm-4 game-card-custom'>
                  <div className='card-header card-header-custom'>
                    <h4 className='card-header-text-custom'>HOME</h4>
                  </div>
                  <div className='card-body game-custom'>
                    <ul className='list-unstyled mt-3 mb-4 attribute-list-custom'>
                      <li>
                        <Link
                          to={{
                            pathname: '/team',
                            teamId: this.state.data.homeTeamId,
                          }}
                        >
                          {this.state.home_team.name}
                        </Link>
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li
                        className={
                          this.state.data.homeTeamScore >
                          this.state.data.awayTeamScore
                            ? 'win'
                            : 'lose'
                        }
                      >
                        {this.state.data.homeTeamScore}
                      </li>
                    </ul>
                  </div>
                  <div className='card-body game-card-body-custom'>
                    {/******************** PLAYER TABLE ********************/}
                    <div className='table-responsive player-table-custom'>
                      <table
                        className='table table-dark table-striped table-sm'
                        style={{ backgroundColor: '#111' }}
                      >
                        <thead>
                          <tr>
                            <th>Players</th>
                          </tr>
                        </thead>
                        <tbody>
                          {homeTeamPlayerList.map((curr_p, index) => (
                            <tr>
                              <td>
                                <Link
                                  to={{
                                    pathname: '/player',
                                    playerId: curr_p.id,
                                    teamName: this.state.home_team.name,
                                  }}
                                >
                                  {curr_p.name}
                                </Link>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className='col-sm-2 vs-custom'>VS</div>
                <div className='card mb-5 shadow-sm bg-dark col-sm-4 game-card-custom'>
                  <div className='card-header card-header-custom'>
                    <h4 className='card-header-text-custom'>AWAY</h4>
                  </div>
                  <div className='card-body game-custom'>
                    <ul className='list-unstyled mt-3 mb-4 attribute-list-custom'>
                      <li>
                        <Link
                          to={{
                            pathname: '/team',
                            teamId: this.state.awayTeamId,
                          }}
                        >
                          {this.state.away_team.name}
                        </Link>
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li
                        className={
                          this.state.homeTeamScore < this.state.awayTeamScore
                            ? 'win'
                            : 'lose'
                        }
                      >
                        {this.state.data.awayTeamScore}
                      </li>
                    </ul>
                  </div>
                  <div className='card-body game-card-body-custom'>
                    {/******************** PLAYER TABLE ********************/}
                    <div className='table-responsive player-table-custom'>
                      <table
                        className='table table-dark table-striped table-sm'
                        style={{ backgroundColor: '#111' }}
                      >
                        <thead>
                          <tr>
                            <th>Players</th>
                          </tr>
                        </thead>
                        <tbody>
                          {visitorTeamPlayerList.map((curr_p, index) => (
                            <tr>
                              <td>
                                <Link
                                  to={{
                                    pathname: '/player',
                                    playerId: curr_p.id,
                                    teamName: this.state.away_team.name,
                                  }}
                                >
                                  {curr_p.name}
                                </Link>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className='col-sm-1'></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
