import React from 'react';
import '../css/Models.css';
import gamesPic from '../images/games-page-picture.jpg';
import axios from 'axios';
import Pagination from '../components/Pagination.js';
import GamesPage from '../components/GamesPage.js';
import url from '../url.js';

export default class Games extends React.Component {
  state = {
    data: [],
    done: false,
    game_list: [],
    date: false,
    homeTeamIdSort: false,
    homeTeamScoreSort: false,
    awayTeamScoreSort: false,
    awayTeamIdSort: false,
    dateArrow: '-',
    homeTeamIdArrow: '-',
    homeTeamScoreArrow: '-',
    awayTeamScoreArrow: '-',
    awayTeamIdArrow: '-',
    teams: [],
    gamesPerPage: 25,
    currentPage: 0,
    highlight: ''
  };
  async componentDidMount() {
    this.setState({ currentPage: this.props.location.page });
    if (!this.state.currentPage) {
      this.setState({ currentPage: 1 });
    }

    let res = await axios.get(url + 'api/all/teams');
    this.setState({ teams: res.data });

    res = await axios.get(url + 'api/all/games');
    this.setState({ data: res.data, game_list: res.data, done: true });
  }

  onSort(event, sortKey) {
    const data = this.state.data;

    let keyName = sortKey + 'Sort';
    let keyArrow = sortKey + 'Arrow';
    let newArrow = '\u2227';
    if (typeof data[0][sortKey] === 'number') {
      if (this.state[keyName]) {
        data.sort((a, b) => b[sortKey] - a[sortKey]);
        newArrow = '\u2228';
      } else {
        data.sort((a, b) => a[sortKey] - b[sortKey]);
      }
    } else {
      if (this.state[keyName]) {
        data.sort((a, b) => b[sortKey].localeCompare(a[sortKey]));
        newArrow = '\u2228';
      } else {
        data.sort((a, b) => a[sortKey].localeCompare(b[sortKey]));
      }
    }

    this.setState({
      game_list: data,
      [keyName]: !this.state[keyName],
      dateArrow: '-',
      homeTeamIdArrow: '-',
      homeTeamScoreArrow: '-',
      awayTeamScoreArrow: '-',
      awayTeamIdArrow: '-',
      [keyArrow]: newArrow,
    });
  }

  async handleSearch(e) {
    let text = '';
    if (typeof e === 'string') {
      this.props.location.searchText = null;
      text = e;
    } else text = e.target.value;
    this.setState({ highlight: text });
    let glist = this.state.game_list;

    let searchData = await glist.filter((game) =>
      game.date.toLowerCase().includes(text.toLowerCase()) ||
      game.awayTeamScore.toString().toLowerCase().includes(text.toLowerCase()) ||
      game.homeTeamScore.toString().toLowerCase().includes(text.toLowerCase()) ||
      (this.state.teams.find((team) => team.id === game.homeTeamId)).name.toLowerCase().includes(text.toLowerCase()) ||
      (this.state.teams.find((team) => team.id === game.awayTeamId)).name.toLowerCase().includes(text.toLowerCase())
    );

    this.setState({ data: searchData });

  }

  displaySearch(display) {
    if (!display) return <div></div>;
    else
      return (
        <div className='search-items'>
          <label htmlFor='search' className='search-label'>
            Search:
          </label>
          <input
            className='search-input'
            name='search'
            type='text'
            autoComplete='off'
            onChange={(e) => {
              this.handleSearch(e);
            }}
          />
        </div>
      );
  }

  render() {
    let gamesList = this.state.data;
    if (!this.state.done) {
      return <div></div>;
    }

    if (this.props.location.teamId) {
      gamesList = gamesList.filter(
        (game) =>
          game.homeTeamId === this.props.location.teamId ||
          game.awayTeamId === this.props.location.teamId
      );
    }
    const numRows = gamesList.length;

    if (this.props.location.page) {
      gamesList = gamesList.slice(
        (this.props.location.page - 1) * this.state.gamesPerPage,
        this.props.location.page * this.state.gamesPerPage
      );
    } else {
      gamesList = gamesList.slice(
        (this.state.currentPage - 1) * this.state.gamesPerPage,
        this.state.currentPage * this.state.gamesPerPage
      );
    }
    if (this.props.location.searchText)
      this.handleSearch(this.props.location.searchText);
    return (
      <div>
        {/******************** GAME TABLE ********************/}
        <div className='container model-container-custom'>
          <div className='headers-custom'>
            <h2>NBA Games</h2>
            {this.displaySearch(!this.props.location.displaySearch)}
          </div>
          <div className='table-responsive text-center'>
            <table
              className='table table-dark table-striped table-sm'
              style={{ backgroundColor: '#111' }}
            >
              <thead>
                <tr>
                  <th>
                    Date
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'date')}
                    >
                      {this.state.dateArrow}
                    </button>
                  </th>
                  <th>
                    Home Team
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'homeTeamId')}
                    >
                      {this.state.homeTeamIdArrow}
                    </button>
                  </th>
                  <th>
                    Home Team Score
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'homeTeamScore')}
                    >
                      {this.state.homeTeamScoreArrow}
                    </button>
                  </th>
                  <th>-</th>
                  <th>
                    Away Team Score
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'awayTeamScore')}
                    >
                      {this.state.awayTeamScoreArrow}
                    </button>
                  </th>
                  <th>
                    Away Team
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'awayTeamId')}
                    >
                      {this.state.awayTeamIdArrow}
                    </button>
                  </th>
                </tr>
              </thead>
              <tbody>
                {gamesList.map((curr_g) => (
                  <GamesPage game={curr_g} teams={this.state.teams} highlight={this.state.highlight}/>
                ))}
              </tbody>
            </table>
            <Pagination
              rowsPerPage={25}
              totalRows={numRows}
              path={
                this.props.location.path === '/search' ? '/search' : '/games'
              }
              id={this.props.location.teamId}
            />
          </div>
          <img src={gamesPic} alt='nba games' />
        </div>
      </div>
    );
  }
}
