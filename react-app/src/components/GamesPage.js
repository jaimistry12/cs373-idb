import React from 'react';
import { Link } from 'react-router-dom';
import Highlight from 'react-highlighter';

const GamesPage = ({ game, teams, highlight }) => {
  const homeTeam = teams.find((team) => team.id === game.homeTeamId);
  const awayTeam = teams.find((team) => team.id === game.awayTeamId);

  game.date = '' + game.date;
  game.homeTeamScore = '' + game.homeTeamScore;
  game.awayTeamScore = '' + game.awayTeamScore;
  return (
    <tr>
      <td>
        <Link
          to={{
            pathname: '/game',
            gameId: game.id,
          }}
        >
          <Highlight search={highlight}>{game.date.substring(0, 10)}</Highlight>
        </Link>
      </td>
      <td>
        <Link
          to={{
            pathname: '/team',
            teamId: game.homeTeamId,
          }}
        >
          <Highlight search={highlight}>{homeTeam.name}</Highlight>
        </Link>
      </td>
      <td className={game.homeTeamScore > game.awayTeamScore ? 'win' : 'lose'}>
        <Highlight search={highlight}>{game.homeTeamScore}</Highlight>
      </td>
      <td>-</td>
      <td className={game.homeTeamScore < game.awayTeamScore ? 'win' : 'lose'}>
        <Highlight search={highlight}>{game.awayTeamScore}</Highlight>
      </td>
      <td>
        <Link
          to={{
            pathname: '/team',
            teamId: game.awayTeamId,
          }}
        >
          <Highlight search={highlight}>{awayTeam.name}</Highlight>
        </Link>
      </td>
    </tr>
  );
};

export default GamesPage;
