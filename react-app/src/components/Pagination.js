import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Pagination.css';

const Pagination = ({ rowsPerPage, totalRows, path, id }) => {
  const rowNumbers = [];

  for (let i = 1; i <= Math.ceil(totalRows / rowsPerPage); i++) {
    rowNumbers.push(i);
  }
  //console.log(path);
  return (
    <nav>
      <ul className='pagination pagination-custom'>
        {rowNumbers.map((number) => (
          <li key={number} className='page-item page-item-custom'>
            <Link to={{ pathname: path, page: number, teamId: id }}>
              {' '}
              {number}{' '}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;
