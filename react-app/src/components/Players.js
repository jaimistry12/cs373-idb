import React from 'react';
import '../css/Models.css';
import playersPic from '../images/players-page-picture.jpg';
import axios from 'axios';
import Pagination from '../components/Pagination.js';
import PlayersPage from '../components/PlayersPage.js';
import url from '../url.js';

export default class Players extends React.Component {
  state = {
    data: {},
    p_list: [],
    done: false,
    nameSort: false,
    teamIdSort: false,
    positionSort: false,
    height_inchesSort: false,
    weightSort: false,
    nameArrow: '-',
    teamIdArrow: '-',
    positionArrow: '-',
    height_inchesArrow: '-',
    weightArrow: '-',
    p_done: false,
    playersPerPage: 25,
    currentPage: 0,
    currentPlayers: {},
    highlight: '',
  };

  async componentDidMount() {
    this.setState({ currentPage: this.props.location.page });
    if (!this.state.currentPage) {
      this.setState({ currentPage: 1 });
    }

    let res = await axios.get(url + 'api/all/teams');
    this.setState({ teams: res.data });

    res = await axios.get(url + 'api/all/players');
    this.setState({
      data: res.data,
      p_list: res.data,
      currentPlayers: res.data.slice(
        (this.state.currentPage - 1) * this.state.playersPerPage,
        this.state.currentPage * this.state.playersPerPage
      ),
      done: true,
    });
  }

  async onSort(event, sortKey) {
    const data = this.state.data;

    let keyName = sortKey + 'Sort';
    let keyArrow = sortKey + 'Arrow';
    let newArrow = '\u2227';
    if (sortKey == 'teamId') {
      if (this.state[keyName]) {
        data.sort((a, b) => b[sortKey] - a[sortKey]);
        newArrow = '\u2228';
      } else {
        data.sort((a, b) => a[sortKey] - b[sortKey]);
      }
    } else {
      if (this.state[keyName]) {
        data.sort((a, b) => ('' + b[sortKey]).localeCompare('' + a[sortKey]));
        newArrow = '\u2228';
      } else {
        data.sort((a, b) => ('' + a[sortKey]).localeCompare('' + b[sortKey]));
      }
    }

    this.setState({
      data,
      currentPlayers: data.slice(
        (this.state.currentPage - 1) * this.state.playersPerPage,
        this.state.currentPage * this.state.playersPerPage
      ),
      [keyName]: !this.state[keyName],
      nameArrow: '-',
      teamIdArrow: '-',
      positionArrow: '-',
      height_inchesArrow: '-',
      weightArrow: '-',
      [keyArrow]: newArrow,
    });
  }

  async handleSearch(e) {
    let text = '';
    if (typeof e === 'string') {
      this.props.location.searchText = null;
      text = e;
    } else text = '' + e.target.value;
    this.setState({ highlight: text });
    console.log(typeof this.state.highlight);
    let plist = this.state.p_list;
    //add weight, team name
    // let team = this.state.teams.find((team) => team.id === player.teamId);
    let searchData = await plist.filter(
      (player) =>
        player.name.toLowerCase().includes(text.toLowerCase()) ||
        player.position.toLowerCase().includes(text.toLowerCase()) ||
        player.height.toString().toLowerCase().includes(text.toLowerCase()) ||
        (player.weight &&
          player.weight
            .toString()
            .toLowerCase()
            .includes(text.toLowerCase())) ||
        this.state.teams
          .find((team) => team.id === player.teamId)
          .name.toLowerCase()
          .includes(text.toLowerCase())
    );
    this.setState({
      data: searchData,
      currentPlayers: searchData.slice(
        (this.state.currentPage - 1) * this.state.playersPerPage,
        this.state.currentPage * this.state.playersPerPage
      ),
    });
  }

  displaySearch(display) {
    if (!display) return <div></div>;
    else
      return (
        <div className='search-items'>
          <label htmlFor='search' className='search-label'>
            Search:
          </label>
          <input
            className='search-input'
            name='search'
            type='text'
            autoComplete='off'
            onChange={(e) => {
              this.handleSearch(e);
            }}
          />
        </div>
      );
  }

  render() {
    let playerList = this.state.currentPlayers;

    if (!this.state.done) {
      return <div></div>;
    }

    if (this.props.location.page) {
      playerList = this.state.data.slice(
        (this.props.location.page - 1) * this.state.playersPerPage,
        this.props.location.page * this.state.playersPerPage
      );
    }
    if (this.props.location.searchText)
      this.handleSearch(this.props.location.searchText);
    return (
      <div>
        {/******************** PLAYER TABLE ********************/}
        <div className='container model-container-custom'>
          <div className='headers-custom'>
            <h2>NBA Players</h2>
            {this.displaySearch(!this.props.location.displaySearch)}
          </div>

          <div className='table-responsive text-center'>
            <table
              className='table table-dark table-striped table-sm'
              style={{ backgroundColor: '#111' }}
            >
              <thead>
                <tr>
                  <th>
                    Name
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'name')}
                    >
                      {this.state.nameArrow}
                    </button>
                  </th>
                  <th>
                    Team
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'teamId')}
                    >
                      {this.state.teamIdArrow}
                    </button>
                  </th>
                  <th>
                    Position
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'position')}
                    >
                      {this.state.positionArrow}
                    </button>
                  </th>
                  <th>
                    Height (ft-in)
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'height_inches')}
                    >
                      {this.state.height_inchesArrow}
                    </button>
                  </th>
                  <th>
                    Weight (lb)
                    <button
                      className='table-header-button-custom'
                      onClick={(e) => this.onSort(e, 'weight')}
                    >
                      {this.state.weightArrow}
                    </button>
                  </th>
                </tr>
              </thead>
              <tbody>
                {playerList.map((curr_p) => (
                  <PlayersPage
                    player={curr_p}
                    teams={this.state.teams}
                    highlight={this.state.highlight}
                  />
                ))}
              </tbody>
            </table>

            <Pagination
              rowsPerPage={25}
              totalRows={this.state.data.length}
              path={
                this.props.location.path === '/search' ? '/search' : '/players'
              }
            />
          </div>
          <img src={playersPic} alt='nba players' />
        </div>
      </div>
    );
  }
}
