import React from 'react';
import { Link } from 'react-router-dom';
import Highlight from 'react-highlighter';

const PlayersPage = ({ player, teams, highlight }) => {
  const team = teams.find((team) => team.id === player.teamId);
  player.weight = '' + player.weight;

  return (
    <tr>
      <td>
        <Link
          to={{
            pathname: '/player',
            playerId: player.id,
            teamName: team.name,
          }}
        >
          <Highlight search={highlight}>{player.name}</Highlight>
        </Link>
      </td>
      <td>
        <Link
          to={{
            pathname: '/team',
            teamId: player.teamId,
          }}
        >
          <Highlight search={highlight}>{team.name}</Highlight>
        </Link>
      </td>
      <td>
        <Highlight search={highlight}>
          {player.position == '' ? 'Unlisted' : player.position}
        </Highlight>
      </td>
      <td>
        <Highlight search={highlight}>
          {player.height == 'None - None' ? 'Unlisted' : player.height}
        </Highlight>
      </td>
      <td>
        <Highlight search={highlight}>
          {player.weight == 'null' ? 'Unlisted' : player.weight}
        </Highlight>
      </td>
    </tr>
  );
};

export default PlayersPage;
