import React from 'react';
import '../css/Search.css';
import Players from '../components/Players';
import Teams from '../components/Teams';
import Games from '../components/Games';

export default class Search extends React.Component {
  state = { selectedModel: '', searchText: '' };

  onSearchButtonClick = (model) => {
    this.setState({ selectedModel: model });
  };

  async handleInput(e) {
    await this.setState({ searchText: e.target.value });
  }

  searchPage = (selectedModel) => {
    let path = '/search';
    let page = this.props.location.page;
    if (selectedModel === 'players')
      return (
        <Players
          location={{
            path,
            page,
            displaySearch: true,
            searchText: this.state.searchText,
          }}
        />
      );
    else if (selectedModel === 'teams')
      return (
        <Teams
          location={{ displaySearch: true, searchText: this.state.searchText }}
        />
      );
    else if (selectedModel === 'games')
      return (
        <Games
          location={{
            path,
            page,
            displaySearch: true,
            searchText: this.state.searchText,
          }}
        />
      );
    else return <div>hi</div>;
  };

  render() {
    return (
      <div>
        {/******************** JUMBOTRON ********************/}
        <div className='jumbotron jumbotron-fluid jumbo-search-custom'>
          <div className='container' style={{ textAlign: 'center' }}>
            <label
              htmlFor='search'
              className='display-3 jumbo-search-title-custom search-label'
            >
              Search a Player, Team or Game
            </label>
            <input
              className='search-input-field'
              name='search'
              type='text'
              autoComplete='off'
              onChange={(e) => {
                this.handleInput(e);
              }}
            />
            <div className='row'>
              <div className='col-sm-2'></div>
              <button
                onClick={() =>
                  this.onSearchButtonClick('players', this.state.searchText)
                }
                className='btn btn-dark btn-lg col-sm-2 jumbo-button-custom'
              >
                Players
              </button>
              <div className='col-sm-1'></div>
              <button
                onClick={() =>
                  this.onSearchButtonClick('teams', this.state.searchText)
                }
                className='btn btn-dark btn-lg col-sm-2 jumbo-button-custom'
              >
                Teams
              </button>
              <div className='col-sm-1'></div>
              <button
                onClick={() =>
                  this.onSearchButtonClick('games', this.state.searchText)
                }
                className='btn btn-dark btn-lg col-sm-2 jumbo-button-custom'
              >
                Games
              </button>
              <div className='col-sm-2'></div>
            </div>
          </div>
        </div>
        {/******************** PLAYER TABLE ********************/}
        {this.searchPage(this.state.selectedModel)}
      </div>
    );
  }
}
