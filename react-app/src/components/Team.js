import React from 'react';
import { Link } from 'react-router-dom';
import '../css/Team.css';
import axios from 'axios';
import url from '../url.js';

export default class Team extends React.Component {
  state = {
    data: [],
    done: false,
    playerList: [],
  };

  async componentDidMount() {
    let res = await axios.get(
      url + `api/teams/id/${this.props.location.teamId}`
    );
    this.setState({ data: res.data });

    let playerData = await axios.get(url + 'api/all/players');

    let playerList = playerData.data.filter(
      (player) => player.teamId === this.props.location.teamId
    );

    this.setState({ playerList, done: true });
  }
  render({ id, name, abbr, division, city, conference } = this.state.data) {
    if (!this.state.done) return <div></div>;

    return (
      <div style={{ color: 'white' }}>
        {/******************** TEAM DATA ********************/}
        <Link className='btn btn-dark back-button-custom' to='/teams'>
          &laquo; Back to Teams
        </Link>
        <div className='container'>
          <div className='card-deck text-center content-custom'>
            <div className='container'>
              <div className='row' style={{ marginTop: '30px' }}>
                <div className='card mb-4 shadow-sm col-sm-7 bg-dark card-header-custom'>
                  <div className='card-header card-header-text-custom'>
                    <h4>{name}</h4>
                  </div>
                  <div className='card-body team-custom'>
                    <ul className='list-unstyled mt-3 mb-4 attribute-list-custom'>
                      <li>
                        <b>Abbreviation:</b> {abbr}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Conference:</b> {conference}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>Division:</b> {division}
                      </li>
                      <div className='attribute-separator'>-----------</div>
                      <li>
                        <b>City:</b> {city}
                      </li>
                    </ul>
                  </div>
                </div>
                {/******************** GAMES LINK ********************/}
                <div className='right column col-sm-5'>
                  <div className='card mb-5 shadow-sm bg-dark games-link-custom'>
                    <Link
                      to={{
                        pathname: '/games',
                        teamId: this.state.data.id,
                      }}
                    >
                      Past Games
                    </Link>
                  </div>
                  {/******************** PLAYER TABLE ********************/}
                  <div className='table-responsive player-table-custom'>
                    <table
                      className='table table-dark table-striped table-sm'
                      style={{ backgroundColor: '#111' }}
                    >
                      <thead>
                        <tr>
                          <th>Players</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.playerList.map((curr_p, index) => (
                          <tr>
                            <td>
                              <Link
                                to={{
                                  pathname: '/player',
                                  playerId: curr_p.id,
                                  teamName: name,
                                }}
                              >
                                {curr_p.name}
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
